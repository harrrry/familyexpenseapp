//
//  HPDetailTableViewController.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 6/4/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import UIKit
import CoreData

class HPDetailTableViewController: CoreDataTableViewController {
    
    var docController: UIDocumentInteractionController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsSelection = false
        updateUI()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(HPLayoutConstants.kAmountDetailCell, forIndexPath: indexPath) as! HPDetailTableViewCell
        
        if let item = fetchedResultsController?.objectAtIndexPath(indexPath) as? Item {
            cell.amount = item.amount
            cell.name = item.name
            cell.weekdayImage = weekdayImageFromString(item.added?.week)
            cell.day = Int((item.added?.day)!)
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 66
    }
    
    override func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        return nil
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            if let item = fetchedResultsController?.objectAtIndexPath(indexPath) as? Item {
                HPLayoutConstants.managedObjectContext?.deleteObject(item)
                if let appDelegate = HPLayoutConstants.appDelegate {
                    appDelegate.saveContext()
                }
                if let amount = item.amount {
                    NSUserDefaults.add(amount: amount)
                }
            }
        }
    }
    
    private func updateUI() {
        if let context = HPLayoutConstants.managedObjectContext {
            let request = NSFetchRequest(entityName: "Item")
            request.sortDescriptors = [NSSortDescriptor(
                    key: "added.timestamp",
                    ascending: false,
                    selector: #selector(NSString.localizedStandardCompare(_:))
                ), NSSortDescriptor(
                    key: "added.day",
                    ascending: false,
                    selector: #selector(NSString.localizedStandardCompare(_:))
                ), NSSortDescriptor(
                    key: "name",
                    ascending: true,
                    selector: #selector(NSString.localizedCaseInsensitiveCompare(_:))
                )]
            fetchedResultsController = NSFetchedResultsController(
                fetchRequest: request,
                managedObjectContext: context,
                sectionNameKeyPath: "added.timestamp",
                cacheName: nil
            )
        } else {
            fetchedResultsController = nil
        }
    }
    
    private func weekdayImageFromString(week: String?) -> WeekdayImage? {
        guard let week = week else {
            return nil
        }
        
        var weekdayImage: WeekdayImage?
        switch week {
        case "Sun":
            weekdayImage = WeekdayImage.Sunday
        case "Mon":
            weekdayImage = WeekdayImage.Monday
        case "Tue":
            weekdayImage = WeekdayImage.Tuesday
        case "Wed":
            weekdayImage = WeekdayImage.Wednesday
        case "Thu":
            weekdayImage = WeekdayImage.Thursday
        case "Fri":
            weekdayImage = WeekdayImage.Friday
        case "Sat":
            weekdayImage = WeekdayImage.Saturday
        default:
            weekdayImage = nil
        }
        return weekdayImage
    }
    
    // MARK: ExportExcel
    
    @IBAction func exportCSV(sender: UIBarButtonItem) {
        createCSVFile()
    }
    
    func createCSVFile() {
        guard let dates = HPDetailController.dates else {
            return
        }
        var csv = "Date,Item,Amount\n"
        
        for date in dates {
            guard let year = date.year,
                month = date.month,
                day = date.day,
                items = date.items else {
                    return
            }
            
            let dateString = "\(year)-\(month)-\(day)"
            
            for item in items {
                guard let item = item as? Item, let name = item.name, amount = item.amount else {
                    continue
                }
                
                csv += "\(dateString),\(name),\(amount)\n"
            }
        }
        
        let path = NSTemporaryDirectory().stringByAppendingString("\(HPLayoutConstants.kExcelName)-\(NSDate.currentShortDateString()).\(HPLayoutConstants.kExcelExtension)")
        // Write File
        
        do {
            try csv.writeToFile(path, atomically: true, encoding: NSUTF8StringEncoding)
            
            let fileURL = NSURL(fileURLWithPath: path)
            docController = UIDocumentInteractionController(URL: fileURL)
            docController?.presentOptionsMenuFromRect(self.view.frame, inView:self.view, animated:true)
        } catch {
            
            print("Failed to create file")
            print("\(error)")
        }
    }
}
