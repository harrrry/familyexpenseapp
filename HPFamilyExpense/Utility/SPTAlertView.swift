//
//  SPTAlertView.swift
//  Printable
//
//  Created by Harry on 1/3/2016.
//  Copyright © 2016 Snaptee. All rights reserved.
//

import UIKit

class SPTAlertView {

    private let margin = CGFloat(10)

    private var alertView = UIView()
    private var label = UILabel()
    private var spinnerView = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)

    private var isAutoDismiss = false

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    init() {
        setup()
    }


    func setup() {
        alertView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.9)
        alertView.layer.cornerRadius = 10
        alertView.clipsToBounds = true

        spinnerView.startAnimating()

        label = UILabel()
        label.textAlignment = .Center
        label.textColor = UIColor.whiteColor()
        alertView.addSubview(label)
    }

    func showAlert(message: String, spinner: Bool, duration: NSTimeInterval) {
        showAlert(message, spinner: spinner)

        isAutoDismiss = true

        let window = UIApplication.sharedApplication().delegate?.window
        window??.userInteractionEnabled = true

        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(duration * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            if self.isAutoDismiss {
                self.hideAlert()
            }
        }
    }

    func showAlert(message: String, spinner: Bool) {
        isAutoDismiss = false

        alertView.alpha = 0
        label.text = message
        label.sizeToFit()

        let appFrame = UIScreen.mainScreen().bounds
        var width = CGFloat(0)
        var height = CGFloat(0)
        var alertViewSize = CGSizeZero

        if spinner {
            alertView.addSubview(spinnerView)

            width = label.bounds.size.width > spinnerView.bounds.size.width ? label.bounds.size.width + 2 * margin : spinnerView.bounds.size.width + 2 * margin
            height = label.bounds.size.height + spinnerView.bounds.size.height + 2 * margin
            alertViewSize = CGSizeMake(width, height)
            updateAlertViewFrame(CGRectMake(appFrame.size.width / 2 - alertViewSize.width / 2, appFrame.size.height / 2 - alertViewSize.height / 2, alertViewSize.width, alertViewSize.height))

            let center = CGRectGetCenter(alertView.bounds)
            spinnerView.center = CGPointMake(center.x, margin + spinnerView.bounds.size.height / 2)

            label.center = CGPointMake(spinnerView.center.x, spinnerView.center.y + spinnerView.bounds.size.height)
        } else {
            spinnerView.removeFromSuperview()

            width = label.bounds.size.width + 2 * margin
            height = label.bounds.size.height + 2 * margin
            alertViewSize = CGSizeMake(width, height)
            updateAlertViewFrame(CGRectMake(appFrame.size.width / 2 - alertViewSize.width / 2, appFrame.size.height / 2 - alertViewSize.height / 2, alertViewSize.width, alertViewSize.height))

            label.center = CGRectGetCenter(alertView.bounds)
        }

        let window = UIApplication.sharedApplication().delegate?.window
        window??.addSubview(alertView)
        window??.userInteractionEnabled = false
        UIView.animateWithDuration(0.3) { () -> Void in
            self.alertView.alpha = 1;
        }
    }

    func hideAlert() {
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.alertView.alpha = 0
            }) { (b) -> Void in
                let window = UIApplication.sharedApplication().delegate?.window
                window??.userInteractionEnabled = true
                self.alertView.removeFromSuperview()
        }
    }

    private func updateAlertViewFrame(frame: CGRect) {
        var resizedFrame = frame
        if frame.size.width < frame.size.height {
            resizedFrame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.height, frame.size.height)
        }
        alertView.frame = resizedFrame
    }

}


extension SPTAlertView {
    func CGRectGetCenter(rect: CGRect) -> CGPoint {
        return CGPointMake(rect.origin.x + rect.size.width / 2,
                           rect.origin.y + rect.size.height / 2)
    }
}
