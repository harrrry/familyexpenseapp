//
//  HPUtility.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 5/28/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import Foundation
import UIKit
import CoreData

struct HPLayoutConstants {
    static let kNavigationBarTintColor = UIColor(hexString:"#FF9B81", alpha: 1)
    static let kNavigationBarBgColor = UIColor(hexString:"#F2DED1", alpha: 1)
    static let kNavigationTitleColor = UIColor(hexString:"#4D4D4D", alpha: 1)

    static let kButtonFontColor = UIColor(hexString:"#4D4D4D", alpha: 1)
    static let kButtonBgColor = UIColor(hexString:"#F1C2AF", alpha: 1)
    
    static let kLabelTextColor = UIColor(hexString:"#F1C20A", alpha: 1)

    static let kTableViewBgColor = UIColor(hexString:"#FFECE1", alpha: 0.3)
    
    static let kPopupViewHeight: CGFloat = 106 + kPopupViewPadding.top + kPopupViewPadding.bottom
    static let kPopupViewPadding = UIEdgeInsetsMake(10, 10, 10, 10)
    static let kPopupViewSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width -
        kPopupViewPadding.left -
        kPopupViewPadding.right,
                                           kPopupViewHeight)
    static let kPopupViewOrigin = CGPointMake((UIScreen.mainScreen().bounds.size.width - kPopupViewSize.width) / 2,
                                              (UIScreen.mainScreen().bounds.size.height - kPopupViewSize.height) / 3)
    static let kPopupViewBgColor = UIColor(hexString:"F1C2AF", alpha: 0.9)
    
    static let kItemAmountKey = "amount"
    static let kItemDetailKey = "detail"
    static let kItemNameKey = "name"
    static let kItemTimeIntervalKey = "timeInterval"
    static let kAddedDateKey = "addedDate"
    
    static let kAmountDetailCell = "AmountDetailCell"

    static let kUpdateBalanceNotification = "UpdateBalanceNotification"
    
    static let kExcelName = "Ivy&Harry"
    static let kExcelExtension = "csv"
    
    static var managedObjectContext: NSManagedObjectContext? {
        get {
            guard let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate else {
                return nil
            }
            
            if appDelegate.respondsToSelector(Selector("managedObjectContext")) {
                return appDelegate.managedObjectContext
            }
            
            return nil
        }
    }

    static var appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
    
    static let kUserDefaultsRemainKey = "RemainKey"
}


extension UIColor {

    convenience init(hexString: String, alpha: CGFloat) {
        var rgbValue: CUnsignedInt = 0
        let scanner = NSScanner(string: hexString)
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        scanner.scanHexInt(&rgbValue)
        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255,
                  green: CGFloat((rgbValue & 0xFF00) >> 8) / 255,
                  blue: CGFloat(rgbValue & 0xFF) / 255,
                  alpha: alpha)
    }
}

extension UIView {
    func changeBackgroundColor(color: UIColor) {
        let bgColorLayer = CALayer()
        bgColorLayer.frame = self.layer.frame
        bgColorLayer.backgroundColor = color.CGColor
        self.layer.addSublayer(bgColorLayer)
    }
}

extension NSUserDefaults {
    class func savedRemain() -> NSDecimalNumber {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        var result = NSDecimalNumber.zero()
        
        if let remain = defaults.objectForKey(HPLayoutConstants.kUserDefaultsRemainKey) as? NSDecimalNumber {
            // read savedRemain
            result = remain
        } else {
            // create one
            result = loadAmountFromDB()
            defaults.setObject(result, forKey: HPLayoutConstants.kUserDefaultsRemainKey)
            defaults.synchronize()
        }
        
        return result
    }
    
    class func add(amount amount: NSDecimalNumber) {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        var result = amount
        // check has savedRemain
        if let remain = defaults.objectForKey(HPLayoutConstants.kUserDefaultsRemainKey) as? NSDecimalNumber {
            result = remain.decimalNumberByAdding(amount)
        } else {
            result = result.decimalNumberByAdding(loadAmountFromDB())
        }
        
        // save amount
        defaults.setObject(result, forKey: HPLayoutConstants.kUserDefaultsRemainKey)
        defaults.synchronize()
        
        NSNotificationCenter.defaultCenter().postNotificationName(HPLayoutConstants.kUpdateBalanceNotification,
                                                                  object: self)
    }
    
    private class func loadAmountFromDB() -> NSDecimalNumber {
        var result = NSDecimalNumber.zero()
        if let items = HPDetailController.items {
            for item in items {
                guard let amount = item.amount else {
                    continue
                }
                
                result = result.decimalNumberByAdding(amount)
            }
        }
        
        return result
    }
}