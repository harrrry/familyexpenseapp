//
//  HPDetailController.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 6/5/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import Foundation
import CoreData

class HPDetailController {
    
    class var items: [Item]? {
        get {
            guard let context = HPLayoutConstants.managedObjectContext else {
                return nil
            }
            
            let request = NSFetchRequest(entityName: "Item")
            request.sortDescriptors = [NSSortDescriptor(
                key: "added.timestamp",
                ascending: false,
                selector: #selector(NSString.localizedStandardCompare(_:))
                ), NSSortDescriptor(
                    key: "added.day",
                    ascending: false,
                    selector: #selector(NSString.localizedStandardCompare(_:))
                )]
            
            do {
                if let result = try context.executeFetchRequest(request) as? [Item] {
                    return result
                } else {
                    return nil
                }
            } catch let error as NSError {
                print("Fetch Items Failed! \(error)")
                return nil
            }
        }
    }
    
    class var dates: [Date]? {
        get {
            guard let context = HPLayoutConstants.managedObjectContext else {
                return nil
            }
            
            let request = NSFetchRequest(entityName: "Date")
            request.sortDescriptors = [NSSortDescriptor(
                key: "timestamp",
                ascending: false,
                selector: #selector(NSString.localizedStandardCompare(_:))
                ), NSSortDescriptor(
                    key: "day",
                    ascending: true,
                    selector: #selector(NSString.localizedStandardCompare(_:))
                )]
            
            do {
                if let result = try context.executeFetchRequest(request) as? [Date] {
                    return result
                } else {
                    return nil
                }
            } catch let error as NSError {
                print("Fetch Items Failed! \(error)")
                return nil
            }
        }
    }
    
    class func itemsInCurrentWeek() -> [Item] {
        var result = [Item]()
        
        guard let (startDate, endDate) = datesOfCurrentWeek() else {
            return result
        }
        
        result = itemsBetween(startDate: startDate, endDate: endDate)
        
        return result
    }
    
    class func itemsInCurrentMonth() -> [Item] {
        var result = [Item]()
        
        guard let (startDate, endDate) = datesOfCurrentMonth() else {
            return result
        }
        
        result = itemsBetween(startDate: startDate, endDate: endDate)
        
        return result
    }
    
    class func itemDatesInCurrentMonth() -> [Date] {
        var result = [Date]()
        
        guard let (startDate, endDate) = datesOfCurrentMonth() else {
            return result
        }
        
        result = ItemDatesBetween(startDate: startDate, endDate: endDate)
        
        return result
    }
    
    class func ItemDatesBetween(startDate startDate: NSDate, endDate: NSDate) -> [Date] {
        var result = [Date]()
        if let dates = dates {
            for date in dates {
                if let savedDate = date.date {
                    if savedDate.compare(startDate) != .OrderedAscending &&
                        savedDate.compare(endDate) != .OrderedDescending {
                        result.append(date)
                    }
                }
            }
        }
        return result
    }
    
    class func itemsBetween(startDate startDate: NSDate, endDate: NSDate) -> [Item] {
        var result = [Item]()
        if let items = items {
            for item in items {
                if let date = item.added?.date {
                    if date.compare(startDate) != .OrderedAscending &&
                        date.compare(endDate) != .OrderedDescending {
                        result.append(item)
                    }
                }
            }
        }
        return result
    }
    
    private class func datesOfCurrentWeek() -> (NSDate, NSDate)? {
        let calendar = NSCalendar.currentCalendar()
        var startDate: NSDate?
        var endDate: NSDate?
        var interval = NSTimeInterval(0)
        calendar.rangeOfUnit(.WeekOfMonth, startDate: &startDate, interval: &interval, forDate: NSDate().localeDate())
        endDate = startDate?.dateByAddingTimeInterval(interval - 1)
        
        if let startDate = startDate, endDate = endDate {
            return (startDate.localeDate(), endDate.localeDate())
        } else {
            return nil
        }
    }
    
    private class func datesOfCurrentMonth() -> (NSDate, NSDate)? {
        let calendar = NSCalendar.currentCalendar()
        var startDate: NSDate?
        var endDate: NSDate?
        var interval = NSTimeInterval(0)
        calendar.rangeOfUnit(.Month, startDate: &startDate, interval: &interval, forDate: NSDate().localeDate())
        endDate = startDate?.dateByAddingTimeInterval(interval - 1)
        
        if let startDate = startDate, endDate = endDate {
            return (startDate.localeDate(), endDate.localeDate())
        } else {
            return nil
        }
    }
    
}
