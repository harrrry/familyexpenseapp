//
//  HPDashboardViewController.swift
//  HPFamilyExpense
//
//  Created by Harry on 31/5/2016.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import UIKit

class HPDashboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    private struct Constants {
        static let defaultRowHeight: CGFloat = 44
        static let datePickerTag = 99
        static let dateStartRow = 0
        static let dateEndRow = 1

        static let datePickerCellIdentifier = "DatePickerCell"
        static let dateLabelCellIdentifier = "DateLabelCell"
        static let chartCellIdentifier = "ChartCell"
        static let titleKey = "title"
        static let dateKey = "date"
    }


    weak var datePicker: UIDatePicker!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateSegmentControl: UISegmentedControl!
    
    
    var chartViewCell: HPBarChartTableViewCell?

    var datePickerIndexPath: NSIndexPath?


    let alertView = SPTAlertView()

    var hasDatePicker: Bool {
        get {
            return datePickerIndexPath != nil
        }
    }

    var isAtCustomMode: Bool {
        get {
            return dateSegmentControl.selectedSegmentIndex == 2
        }
    }

    var items = [[String: String]]()

    var datePickerRowHeight: CGFloat = 0

    // MARK: - ViewController Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.scrollEnabled = false
        self.automaticallyAdjustsScrollViewInsets = false

        tableView.backgroundColor = UIColor.clearColor()

        dateSegmentControl.addTarget(self, action: #selector(HPDashboardViewController.selectOtherDateSegment(_:)),
                                     forControlEvents: .ValueChanged)

        view.changeBackgroundColor(HPLayoutConstants.kTableViewBgColor)

        let datePickerCell = tableView.dequeueReusableCellWithIdentifier(Constants.datePickerCellIdentifier)
        datePickerRowHeight = CGRectGetHeight((datePickerCell?.frame)!)

        items.append([Constants.titleKey: "Start Date", Constants.dateKey: NSDate.oneMonthPriorDateString()])
        items.append([Constants.titleKey: "End Date", Constants.dateKey: NSDate.currentShortDateString()])
        items.append([Constants.titleKey: "Bar Chart"])
        
        let leftSwipteGesture = UISwipeGestureRecognizer(target: self,
                                                         action: #selector(HPDashboardViewController.swipeGestureHandler(_:)))
        leftSwipteGesture.direction = .Left
        view.addGestureRecognizer(leftSwipteGesture)
        
        let rightSwipteGesture = UISwipeGestureRecognizer(target: self,
                                                          action: #selector(HPDashboardViewController.swipeGestureHandler(_:)))
        rightSwipteGesture.direction = .Right
        view.addGestureRecognizer(rightSwipteGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - Utilities
    func hasPicker(forIndexPath indexPath: NSIndexPath) -> Bool {
        var hasPicker = false
        if let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forItem: indexPath.row + 1, inSection: indexPath.section)) {
            let datePicker = cell.viewWithTag(Constants.datePickerTag)
            hasPicker = (datePicker != nil)
        }
        return hasPicker
    }

    func indexPathHasPicker(indexPath: NSIndexPath) -> Bool {
        return datePickerIndexPath != nil && (datePickerIndexPath!.row == indexPath.row)
    }

    func indexPathHasDateLabel(indexPath: NSIndexPath) -> Bool {
        return indexPath.row == Constants.dateStartRow || indexPath.row == Constants.dateEndRow
    }

    func displayDatePickerForRowAtIndexPath(indexPath: NSIndexPath) {
        tableView.beginUpdates()

        var before = false
        if hasDatePicker {
            before = datePickerIndexPath!.row < indexPath.row
        }

        var sameCellClicked = false
        if let _ = datePickerIndexPath {
            sameCellClicked = (datePickerIndexPath!.row - 1 == indexPath.row)
        }
        
        
        tableView.scrollEnabled = !hasDatePicker || !sameCellClicked

        //dismiss any datePicker
        if hasDatePicker {
            tableView.deleteRowsAtIndexPaths([datePickerIndexPath!], withRowAnimation: .Fade)
            datePickerIndexPath = nil
        }

        if !sameCellClicked {
            let rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
            let indexPathToReveal = NSIndexPath(forRow: rowToReveal, inSection: indexPath.section)
            toggleDatePicker(forSelectedIndexPath: indexPathToReveal)
            datePickerIndexPath = NSIndexPath(forItem: indexPathToReveal.row + 1, inSection: indexPathToReveal.section)
        }

        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        tableView.endUpdates()

        updateDatePicker()
    }

    func toggleDatePicker(forSelectedIndexPath indexPath: NSIndexPath) {
        tableView.beginUpdates()

        let indexPathes = [NSIndexPath(forRow: indexPath.row + 1, inSection: indexPath.section)]
        if self.hasPicker(forIndexPath: indexPath) {
            //delete one
            tableView.deleteRowsAtIndexPaths(indexPathes, withRowAnimation: .Fade)
        } else {
            //inset one
            tableView.insertRowsAtIndexPaths(indexPathes, withRowAnimation: .Fade)
        }

        tableView.endUpdates()
    }

    func updateDatePicker() {
        guard let indexPath = datePickerIndexPath else {
            return
        }

        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            if let datePicker = cell.viewWithTag(Constants.datePickerTag) as? UIDatePicker {
                let item: [String: String] = items[indexPath.row - 1];

                datePicker.setDate(NSDate.shortDateFromString(item[Constants.dateKey]!), animated: false)
            }
        }
    }

    func chartCellHeight() -> CGFloat {
        var height = tableView.bounds.height

        if isAtCustomMode {
            height = height - 2 * Constants.defaultRowHeight
        }
        return height
    }

    func numberOfRows() -> Int {
        var count = items.count
        if isAtCustomMode && hasDatePicker {
            count += 1
        } else if !isAtCustomMode {
            count = 1
        }
        return count
    }
    
    func updateChartViewCell() {
        var startDate: NSDate?
        var endDate: NSDate?
        for item in items {
            if let title = item[Constants.titleKey] where title == "Start Date" {
                startDate = NSDate.shortDateFromString(item[Constants.dateKey]!).localeDate()
            } else if let title = item[Constants.titleKey] where title == "End Date" {
                endDate = NSDate.shortDateFromString(item[Constants.dateKey]!).localeDate()
            }
        }
        chartViewCell?.datesAndType = (startDate, endDate, .Custom)
    }

    //MARK: - UITableView Methods

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows()
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        var cellIdentifier = Constants.chartCellIdentifier

        if isAtCustomMode {
            if indexPathHasPicker(indexPath) {
                cellIdentifier = Constants.datePickerCellIdentifier
            } else if indexPathHasDateLabel(indexPath) {
                cellIdentifier = Constants.dateLabelCellIdentifier
            }
        }

        cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as UITableViewCell

        if cellIdentifier == Constants.dateLabelCellIdentifier {
            let item: [String: String] = items[indexPath.row]
            cell?.textLabel?.text = item[Constants.titleKey]
            cell?.detailTextLabel?.text = item[Constants.dateKey]
        } else if cellIdentifier == Constants.chartCellIdentifier {
            let cell = cell as! HPBarChartTableViewCell
            chartViewCell = cell
            switch dateSegmentControl.selectedSegmentIndex {
            case 0:
                cell.datesAndType = (nil, nil, .Week)
            case 1:
                cell.datesAndType = (nil, nil, .Month)
            case 2:
                updateChartViewCell()
            default:
                chartViewCell = nil
                break
            }
        }


        return cell!
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var height = tableView.rowHeight
        if isAtCustomMode {
            if indexPathHasPicker(indexPath) {
                height = datePickerRowHeight
            } else {
                if indexPath.row == numberOfRows() - 1 {
                    height = chartCellHeight()
                } else {
                    height = tableView.rowHeight
                }
            }
        } else {
            if indexPath.row == 0 {
                height = chartCellHeight()
            }
        }
        return height
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if cell?.reuseIdentifier == Constants.dateLabelCellIdentifier {
            displayDatePickerForRowAtIndexPath(indexPath)
        }
    }
    

    //MARK: - Actions
    @IBAction func dateAction(sender: UIDatePicker) {
        guard let indexPath = datePickerIndexPath else {
            return
        }

        let cellIndexPath = NSIndexPath(forRow: indexPath.row - 1, inSection: indexPath.section)
        let cell = tableView.cellForRowAtIndexPath(cellIndexPath)

        let dateString = NSDate.stringFromDate(sender.date)
        // update our data model
        var item = items[cellIndexPath.row]
        item[Constants.dateKey] = dateString
        items[cellIndexPath.row] = item
        // update the cell's date string
        cell?.detailTextLabel?.text = dateString
        
        updateChartViewCell()
    }

    @IBAction func selectOtherDateSegment(segmentControl: UISegmentedControl) {
        tableView.reloadData()
        alertView.showAlert("Loading...", spinner: true, duration: 0.3)
        tableView.scrollEnabled = false
        tableView.allowsSelection = isAtCustomMode
    }

    @IBAction func shareChartView() {
        if let image = chartViewCell?.chartImage {
            let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)

            //New Excluded Activities Code
            activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList]

            activityVC.popoverPresentationController?.sourceView = view
            self.presentViewController(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func swipeGestureHandler(gesture: UISwipeGestureRecognizer) {
        let index = dateSegmentControl.selectedSegmentIndex
        
        if gesture.direction == .Right {
            dateSegmentControl.selectedSegmentIndex =
                dateSegmentControl.selectedSegmentIndex > 0 ? index - 1 : 0
            if index != dateSegmentControl.selectedSegmentIndex {
                dateSegmentControl.sendActionsForControlEvents(.ValueChanged)
            }
        } else if gesture.direction == .Left {
            dateSegmentControl.selectedSegmentIndex =
                dateSegmentControl.selectedSegmentIndex >= dateSegmentControl.numberOfSegments ?
                    dateSegmentControl.numberOfSegments : index + 1
            if index != dateSegmentControl.selectedSegmentIndex {
                dateSegmentControl.sendActionsForControlEvents(.ValueChanged)
            }
        }
    }
}


extension NSDate {
    class func currentShortDateString() -> String {
        let dateFormatter = localDateFormatter()
        return dateFormatter.stringFromDate(NSDate())
    }

    class func oneMonthPriorDateString() -> String {
        let dateFormatter = localDateFormatter()
        let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        let offsetComponents = NSDateComponents()
        offsetComponents.month = -1
        let date = gregorian?.dateByAddingComponents(offsetComponents, toDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
        return dateFormatter.stringFromDate(date!)
    }

    class func localDateFormatter() -> NSDateFormatter {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        return dateFormatter
    }

    class func shortDateFromString(dateString: String) -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.dateFromString(dateString)
        return date == nil ? NSDate() : date!
    }

    class func stringFromDate(date: NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.stringFromDate(date)
        return dateString
    }
    
    func localeDate() -> NSDate {
        let timeInterval = NSTimeZone.localTimeZone().secondsFromGMT
        return self.dateByAddingTimeInterval(NSTimeInterval(timeInterval))
    }
    
    func numberOfDaysUntilDateTime(toDateTime: NSDate, inTimeZone timeZone: NSTimeZone? = nil) -> Int {
        let calendar = NSCalendar.currentCalendar()
        if let timeZone = timeZone {
            calendar.timeZone = timeZone
        }
        
        var fromDate: NSDate?, toDate: NSDate?
        
        calendar.rangeOfUnit(.Day, startDate: &fromDate, interval: nil, forDate: self)
        calendar.rangeOfUnit(.Day, startDate: &toDate, interval: nil, forDate: toDateTime)
        
        let difference = calendar.components(.Day, fromDate: fromDate!, toDate: toDate!, options: [])
        return difference.day
    }
}

