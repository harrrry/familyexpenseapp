//
//  Item+CoreDataProperties.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 6/4/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Item {

    @NSManaged var amount: NSDecimalNumber?
    @NSManaged var detail: String?
    @NSManaged var name: String?
    @NSManaged var timeInterval: NSNumber?
    @NSManaged var added: Date?

}
