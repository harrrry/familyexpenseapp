//
//  Item.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 6/4/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import Foundation
import CoreData


class Item: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    
    class func insertItemInfo(itemInfo: [String: AnyObject], inManagedObjectContext context: NSManagedObjectContext) -> Bool {
        var result = true
        
        if let item = NSEntityDescription.insertNewObjectForEntityForName("Item", inManagedObjectContext: context) as? Item {
            item.amount = itemInfo[HPLayoutConstants.kItemAmountKey] as? NSDecimalNumber
            item.detail = itemInfo[HPLayoutConstants.kItemDetailKey] as? String
            item.name = itemInfo[HPLayoutConstants.kItemNameKey] as? String
            item.timeInterval = itemInfo[HPLayoutConstants.kItemTimeIntervalKey] as? NSNumber
            item.added = itemInfo[HPLayoutConstants.kAddedDateKey] as? Date
            
            do {
                try context.save()
            } catch let error as NSError {
                print("Save item failed?! \(error)")
                result = false
            }
        }
        
        return result
    }
    
    class func deleteItemInfo(itemInfo: [String: AnyObject], inManagedObjectContext context: NSManagedObjectContext) -> Bool {
        var result = false
        
        guard let timeInterval = itemInfo[HPLayoutConstants.kItemTimeIntervalKey] as? NSNumber else {
            return result
        }
        
        let request = NSFetchRequest(entityName: "Item")
        request.predicate = NSPredicate(format: "timeInterval = %@", timeInterval)
        
        if let fetchedDate = (try? context.executeFetchRequest(request))?.first as? Item {
            context.deleteObject(fetchedDate)
            result = true
        }
        
        return result
    }

}
