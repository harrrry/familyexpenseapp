//
//  Date.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 6/4/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import Foundation
import CoreData


class Date: NSManagedObject {
    
    // Insert code here to add functionality to your managed object subclass
    
    var date: NSDate? {
        if let year = year, month = month, day = day {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return dateFormatter.dateFromString("\(year)-\(month)-\(day)")?.localeDate()
        } else {
            return nil
        }
    }
    
    class func dateWitnInfo(infoDate: NSDate, inManagedObjectContext context: NSManagedObjectContext) -> Date? {
        let components = NSCalendar.currentCalendar().components([.Day, .Month, .Year, .Weekday], fromDate: infoDate)
        
        let request = NSFetchRequest(entityName: "Date")
        request.predicate = NSPredicate(format: "timestamp=%@ AND day=%@", infoDate.timestamp(), "\(components.day)")
        do {
            let fetchedDate = try context.executeFetchRequest(request)
            if let date = fetchedDate.first as? Date {
                return date
            }
        } catch let error as NSError {
            print("Fetch Dates Failed: \(error)")
        }

        if let date = NSEntityDescription.insertNewObjectForEntityForName("Date", inManagedObjectContext: context) as? Date {
            date.day = "\(components.day)"
            date.month = "\(components.month)"
            date.year = "\(components.year)"
            date.timestamp = "\(components.year)-\(components.month)"
            date.week = weekdayStringFromInt(components.weekday)
            return date
        }
        return nil
    }
    
    
    private class func weekdayStringFromInt(weekday: Int) -> String {
        let result: String
        switch weekday {
        case 1:
            result = "Sun"
        case 2:
            result = "Mon"
        case 3:
            result = "Tue"
        case 4:
            result = "Wed"
        case 5:
            result = "Thu"
        case 6:
            result = "Fri"
        case 7:
            result = "Sat"
        default:
            result = ""
        }
        return result
    }
}


extension NSDate {
    func timestamp() -> NSString {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-M"
        dateFormatter.locale = NSLocale.currentLocale()
        return dateFormatter.stringFromDate(self)
    }
}