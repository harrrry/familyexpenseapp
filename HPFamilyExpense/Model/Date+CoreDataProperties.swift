//
//  Date+CoreDataProperties.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 6/4/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Date {

    @NSManaged var day: String?
    @NSManaged var month: String?
    @NSManaged var timestamp: String?
    @NSManaged var week: String?
    @NSManaged var year: String?
    @NSManaged var items: NSSet?

}
