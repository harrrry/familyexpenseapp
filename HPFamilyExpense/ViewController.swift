//
//  ViewController.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 5/27/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPopoverPresentationControllerDelegate, HPPopupViewControllerDelegate {

    @IBOutlet weak var balanceLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.changeBackgroundColor(HPLayoutConstants.kTableViewBgColor)

        updateBalanceLabel()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.updateBalanceLabel),
                                                         name: HPLayoutConstants.kUpdateBalanceNotification,
                                                         object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)

//        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        updateBalanceLabel()
    }
    
    
    //MARK: - Button Actions
    
    @IBAction func AddNewIncome(sender: UIButton) {
        let popupDelegate = HPPopupTransitioningDelegate()
        transitioningDelegate = popupDelegate
        let vc = HPPopupViewController()
        vc.delegate = self
        vc.isIncomeView = true
        vc.transitioningDelegate = popupDelegate
        presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func AddNewExpense(sender: UIButton) {
        let popupDelegate = HPPopupTransitioningDelegate()
        transitioningDelegate = popupDelegate
        let vc = HPPopupViewController()
        vc.delegate = self
        vc.transitioningDelegate = popupDelegate
        presentViewController(vc, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .FullScreen;
    }
    
    //MARK: - HPPopupViewControllerDelegate
    func popupViewController(viewController: HPPopupViewController, didCaptureItem item: Item) {
        performSegueWithIdentifier("ShowDetailsSegue", sender: self)
    }

    //MARK: - Update Balance Label
    func updateBalanceLabel() {
        balanceLabel.text = "HK$ \(NSUserDefaults.savedRemain())"
    }

}

