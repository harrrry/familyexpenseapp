//
//  HPBarChartTableViewCell.swift
//  HPFamilyExpense
//
//  Created by Harry on 31/5/2016.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import UIKit
import Charts

enum ChartDataType {
    case Week, Month, Custom
}

class HPBarChartTableViewCell: UITableViewCell {

    private var barChart = BarChartView()

    var chartImage: UIImage? {
        get {
            return barChart.getChartImage(transparent: true)
        }
    }
    
    var datesAndType: (NSDate?, NSDate?, ChartDataType)? {
        didSet {
            updateBarChart()
            setNeedsDisplay()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .None
        
        
        
        let xAxis = barChart.xAxis
        xAxis.labelPosition = .Bottom
        xAxis.drawGridLinesEnabled = false
        
        let leftAxis = barChart.leftAxis
        leftAxis.labelPosition = .OutsideChart
        leftAxis.axisMinValue = 0
        
        let rightAxis = barChart.rightAxis
        rightAxis.enabled = true
        rightAxis.drawGridLinesEnabled = false
        rightAxis.axisMinValue = 0
        
        barChart.descriptionText = ""
        
        contentView.addSubview(barChart)
        
        self.userInteractionEnabled = false
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        barChart.frame = contentView.bounds
    }
    
    
    func updateBarChart() {
        barChart.data = nil
        if let (startDate, endDate, chartDataType) = datesAndType {
            var xVals: [String]?
            var yVals = [BarChartDataEntry]()
            switch chartDataType {
            case .Week:
                xVals = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
                var amountSums = [NSDecimalNumber](count: 7, repeatedValue: NSDecimalNumber.zero())
                let items = HPDetailController.itemsInCurrentWeek()
                for item in items {
                    guard let week = item.added?.week,
                        let amount = item.amount
                        where amount.compare(NSDecimalNumber.zero()) == .OrderedAscending else {
                        continue
                    }
                    switch week {
                    case "Sun":
                        amountSums[0] = amountSums[0].decimalNumberByAdding(amount)
                    case "Mon":
                        amountSums[1] = amountSums[1].decimalNumberByAdding(amount)
                    case "Tue":
                        amountSums[2] = amountSums[2].decimalNumberByAdding(amount)
                    case "Wed":
                        amountSums[3] = amountSums[3].decimalNumberByAdding(amount)
                    case "Thu":
                        amountSums[4] = amountSums[4].decimalNumberByAdding(amount)
                    case "Fri":
                        amountSums[5] = amountSums[5].decimalNumberByAdding(amount)
                    case "Sat":
                        amountSums[6] = amountSums[6].decimalNumberByAdding(amount)
                    default:
                        break
                    }
                }
                for index in 0..<amountSums.count {
                    let sum = amountSums[index]
                    yVals.append(BarChartDataEntry(value: 0 - sum.doubleValue, xIndex: index))
                }
                
            case .Month:
                xVals = [String]()
                let dates = HPDetailController.itemDatesInCurrentMonth()
                var amountSums = [NSDecimalNumber](count: dates.count, repeatedValue: NSDecimalNumber.zero())
                for index in 0..<dates.count {
                    let date = dates[index]
                    guard let items = date.items else {
                        continue
                    }
                    xVals?.append("Day \(date.day!)")
                    for item in items {
                        guard let item = item as? Item, let amount = item.amount
                            where amount.compare(NSDecimalNumber.zero()) == .OrderedAscending else {
                                continue
                        }
                        amountSums[index] = amountSums[index].decimalNumberByAdding(amount)
                    }
                }
                for index in 0..<amountSums.count {
                    let sum = amountSums[index]
                    yVals.append(BarChartDataEntry(value: 0 - sum.doubleValue, xIndex: index))
                }
            case .Custom:
                guard let startDate = startDate, let endDate = endDate else {
                    break
                }
                
                let dates = HPDetailController.ItemDatesBetween(startDate: startDate, endDate: endDate)
                xVals = [String]()
                var amountSums = [NSDecimalNumber](count: dates.count, repeatedValue: NSDecimalNumber.zero())
                for index in 0..<dates.count {
                    let date = dates[index]
                    guard let items = date.items else {
                        continue
                    }
                    xVals?.append("\(date.month!).\(date.day!)")
                    for item in items {
                        guard let item = item as? Item, let amount = item.amount
                            where amount.compare(NSDecimalNumber.zero()) == .OrderedAscending else {
                                continue
                        }
                        amountSums[index] = amountSums[index].decimalNumberByAdding(amount)
                    }
                }
                for index in 0..<amountSums.count {
                    let sum = amountSums[index]
                    yVals.append(BarChartDataEntry(value: 0 - sum.doubleValue, xIndex: index))
                }
            }
            if let xVals = xVals {
                let set = BarChartDataSet(yVals: yVals, label: "Amount")
                set.colors = ChartColorTemplates.colorful()
                barChart.data = BarChartData(xVals: xVals, dataSets: [set])
                barChart.data?.highlightEnabled = false
            }
        }
    }
}
