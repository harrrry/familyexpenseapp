//
//  HPDetailTableViewCell.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 6/4/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import UIKit

enum WeekdayImage {
    case Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
}

class HPDetailTableViewCell: UITableViewCell {
    @IBOutlet weak private var dayLabel: UILabel!
    @IBOutlet weak private var amountLabel: UILabel!
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var weekdayImageView: UIImageView!
    
    var weekdayImage: WeekdayImage?
    var amount: NSDecimalNumber?
    var name: String?
    var day: Int?
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let weekdayImage = weekdayImage,
            amount = amount,
            name = name,
            day = day else {
            return
        }
        
        switch weekdayImage {
        case .Sunday:
            weekdayImageView.image = UIImage(named: "Sunday")
        case .Monday:
            weekdayImageView.image = UIImage(named: "Monday")
        case .Tuesday:
            weekdayImageView.image = UIImage(named: "Tuesday")
        case .Wednesday:
            weekdayImageView.image = UIImage(named: "Wednesday")
        case .Thursday:
            weekdayImageView.image = UIImage(named: "Thursday")
        case .Friday:
            weekdayImageView.image = UIImage(named: "Friday")
        case .Saturday:
            weekdayImageView.image = UIImage(named: "Saturday")
        }
        
        amountLabel.text = "HK$\(amount)"
        nameLabel.text = name
        dayLabel.text = "\(day)"
        
    }
    
}
