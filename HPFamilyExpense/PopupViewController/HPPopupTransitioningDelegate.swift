//
//  HPPopupTransitioningDelegate.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 5/28/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import UIKit

class HPPopupTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {

    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController
        presenting: UIViewController,
        sourceViewController source: UIViewController) -> UIPresentationController? {
        return HPPresentationController(presentedViewController: presented, presentingViewController: presenting)
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = HPPopupAnimatedTransitioning()
        animationController.isPresentation = true
        return animationController
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = HPPopupAnimatedTransitioning()
        animationController.isPresentation = false
        return animationController
    }
}
