//
//  HPPopupTableViewCell.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 5/28/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import UIKit

class HPPopupTableViewCell: UITableViewCell {
    let labelTitle = UILabel()
    let inputTextField = UITextField()
    var keyboardType = UIKeyboardType.Default {
        didSet {
            inputTextField.keyboardType = keyboardType
        }
    }
    
    override func awakeFromNib() {
        labelTitle.translatesAutoresizingMaskIntoConstraints = false
        labelTitle.textColor = HPLayoutConstants.kLabelTextColor
        inputTextField.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(inputTextField)
        contentView.addSubview(labelTitle)
        
        var views = [String: UIView]()
        views["textfield"] = inputTextField
        views["label"] = labelTitle
        
        var allConstraints = [NSLayoutConstraint]()
        
        let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat(
            "H:|-[label(>=10@270)]-[textfield(>=20@250)]-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += horizontalConstraints
        
        let labelVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat(
            "V:|-[label]-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += labelVerticalConstraints
        
        let textFieldVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat(
            "V:|-[textfield]-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += textFieldVerticalConstraints
        
        NSLayoutConstraint.activateConstraints(allConstraints)
    }
}
