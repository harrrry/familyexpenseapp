//
//  HPPopupViewController.swift
//  HPFamilyExpense
//
//  Created by HarryPan on 5/28/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import UIKit
import CoreData

protocol HPPopupViewControllerDelegate {
    
    func popupViewController(viewController: HPPopupViewController, didCaptureItem item: Item)
}

class HPPopupViewController: UIViewController, UITextFieldDelegate {
    
    var delegate: HPPopupViewControllerDelegate?
    
    var cancelPressed = false
    var isIncomeView = false
    
    let itemLabel = UILabel()
    let amountLabel = UILabel()
    let dateLabel = UILabel()
    
    let itemTextField = UITextField()
    let amountTextField = UITextField()
    let dateTextField = UITextField()
    
    var currentDateString: String {
        get {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            dateFormatter.timeZone = NSTimeZone.localTimeZone()
            return dateFormatter.stringFromDate(NSDate())
        }
    }
    
    var formatter: NSDateFormatter {
        get {
            let formatter = NSDateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm"
            formatter.locale = NSLocale.currentLocale()
            return formatter
        }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        modalPresentationStyle = UIModalPresentationStyle.Custom
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    private func splitLine() -> UIView {
        let splitLine = UIView(frame: CGRectMake(0, 0, view.bounds.size.width - 16, 1.5))
        splitLine.backgroundColor = UIColor.grayColor()
        return splitLine
    }
    
    private func setupUI() {
        view.backgroundColor = HPLayoutConstants.kPopupViewBgColor
        
        cancelPressed = false
        
        itemLabel.text = "Item"
        itemLabel.textColor = HPLayoutConstants.kButtonFontColor
        view.addSubview(itemTextField)
        view.addSubview(itemLabel)
        
        amountLabel.text = "Amount"
        amountLabel.textColor = HPLayoutConstants.kButtonFontColor
        amountTextField.keyboardType = .DecimalPad
        view.addSubview(amountTextField)
        view.addSubview(amountLabel)
        
        dateLabel.text = "Date"
        dateLabel.textColor = HPLayoutConstants.kButtonFontColor
        view.addSubview(dateTextField)
        view.addSubview(dateLabel)
        
        itemLabel.translatesAutoresizingMaskIntoConstraints = false
        itemTextField.translatesAutoresizingMaskIntoConstraints = false
        amountLabel.translatesAutoresizingMaskIntoConstraints = false
        amountTextField.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateTextField.translatesAutoresizingMaskIntoConstraints = false
    
        
        itemTextField.borderStyle = .RoundedRect
        amountTextField.borderStyle = .RoundedRect
        dateTextField.borderStyle = .RoundedRect
        
        
        let datePicker = UIDatePicker()
        datePicker.addTarget(self, action: #selector(HPPopupViewController.datePickerValueChanged(_:)), forControlEvents: .ValueChanged)
        datePicker.datePickerMode = .DateAndTime
        dateTextField.placeholder = currentDateString
        dateTextField.inputView = datePicker
        let toolBar = UIToolbar(frame: CGRectMake(0, 0, view.bounds.size.width, 44))
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(HPPopupViewController.cancel(_:)))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(HPPopupViewController.done(_:)))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        toolBar.items = [cancelButton, flexibleSpace, doneButton]
        
        dateTextField.inputAccessoryView = toolBar
        amountTextField.inputAccessoryView = toolBar
        itemTextField.inputAccessoryView = toolBar
        
        dateTextField.returnKeyType = .Next
        amountTextField.returnKeyType = .Next
        
        dateTextField.delegate = self
        amountTextField.delegate = self
        itemTextField.delegate = self
        
        amountTextField.addTarget(self, action: #selector(HPPopupViewController.textFieldDidChangeText(_:)), forControlEvents: .EditingChanged)
        
        //Add Layout Constraints
        var views = [String: UIView]()
        views["itemLabel"] = itemLabel
        views["itemTextField"] = itemTextField
        views["amountLabel"] = amountLabel
        views["amountTextField"] = amountTextField
        views["dateLabel"] = dateLabel
        views["dateTextField"] = dateTextField
        
        let matrics = ["top": HPLayoutConstants.kPopupViewPadding.top,
                       "bottom": HPLayoutConstants.kPopupViewPadding.bottom]
        
        var allConstraints = [NSLayoutConstraint]()
        
        allConstraints += NSLayoutConstraint.constraintsWithVisualFormat(
            "H:|-[itemLabel(>=10@270)]-[itemTextField(>=20@250)]-|",
            options: [.AlignAllCenterY],
            metrics: nil,
            views: views)
        allConstraints += NSLayoutConstraint.constraintsWithVisualFormat(
            "H:|-[amountLabel(>=10@270)]-[amountTextField(>=20@250)]-|",
            options: [.AlignAllCenterY],
            metrics: nil,
            views: views)
        allConstraints += NSLayoutConstraint.constraintsWithVisualFormat(
            "H:|-[dateLabel(>=10@270)]-[dateTextField(>=20@250)]-|",
            options: [.AlignAllCenterY],
            metrics: nil,
            views: views)
        
        allConstraints += NSLayoutConstraint.constraintsWithVisualFormat(
            "V:|-top-[itemLabel]-[amountLabel]-[dateLabel]",
            options: [],
            metrics: matrics,
            views: views)
        allConstraints += NSLayoutConstraint.constraintsWithVisualFormat(
            "V:|-top-[itemTextField]-[amountTextField]-[dateTextField]",
            options: [],
            metrics: matrics,
            views: views)
        
        allConstraints += NSLayoutConstraint.constraintsWithVisualFormat(
            "[itemLabel(==amountLabel)]",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += NSLayoutConstraint.constraintsWithVisualFormat(
            "[amountLabel(==dateLabel)]",
            options: [],
            metrics: nil,
            views: views)
        
        NSLayoutConstraint.activateConstraints(allConstraints)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itemTextField.becomeFirstResponder()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        itemTextField.resignFirstResponder()
        amountTextField.resignFirstResponder()
        dateTextField.resignFirstResponder()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        if cancelPressed {
            return
        }
        
        if dateTextField.text == nil || dateTextField.text == "" {
            dateTextField.text = dateTextField.placeholder
        }
        
        guard let name = itemTextField.text,
            dateString = dateTextField.text,
            let date = formatter.dateFromString(dateString),
            context = HPLayoutConstants.managedObjectContext else {
            return
        }
        
        guard !name.isEmpty else {
            return
        }
        
        let amount = NSDecimalNumber(string: amountTextField.text)
        let addedDate = Date.dateWitnInfo(date, inManagedObjectContext: context)
        
        if let item = NSEntityDescription.insertNewObjectForEntityForName("Item", inManagedObjectContext: context) as? Item {
            item.name = name
            item.timeInterval = NSDate().localeDate().timeIntervalSince1970
            item.amount = amount
            item.added = addedDate

            NSUserDefaults.add(amount: amount)
            
            if let delegate = delegate {
                delegate.popupViewController(self, didCaptureItem: item)
            }
        }
        
        if let appDelegate = HPLayoutConstants.appDelegate {
            appDelegate.saveContext()
        }
        
    }
    
    //MARK - Actions
    @IBAction func done(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
        cancelPressed = true;
    }
    
    @IBAction func datePickerValueChanged(sender: UIDatePicker) {
        dateTextField.text = formatter.stringFromDate(sender.date)
    }
    
    @IBAction func textFieldDidChangeText(textField: UITextField) {
        if isIncomeView {
            return
        }
        if textField.isEqual(amountTextField) {
            guard let text = textField.text else {
                return
            }
            if text.hasPrefix("-") {
                return
            }
            textField.text = "-" + text
        }
    }
    
    //MARK - UITextFieldDelegate Methods
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField.isEqual(dateTextField) {
            textField.text = currentDateString
        } else if textField.isEqual(amountTextField) {
            if isIncomeView {
                return
            }
            guard let text = textField.text else {
                textField.text = "-"
                return
            }
            
            if text.hasPrefix("-") {
                return
            }
            textField.text = "-" + text
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.isEqual(itemTextField) {
            amountTextField.becomeFirstResponder()
        } else if textField.isEqual(amountTextField) {
            dateTextField.becomeFirstResponder()
        }
        return true
    }

}
