//
//  HPFamilyExpenseTests.swift
//  HPFamilyExpenseTests
//
//  Created by HarryPan on 5/27/16.
//  Copyright © 2016 HarryPan. All rights reserved.
//

import XCTest
import CoreData
@testable import HPFamilyExpense

class HPFamilyExpenseTests: XCTestCase {
    var item: [String: AnyObject] = [:]
    
    var managedObjectContext: NSManagedObjectContext? {
        get {
            guard let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate else {
                return nil
            }
            
            if appDelegate.respondsToSelector(Selector("managedObjectContext")) {
                return appDelegate.managedObjectContext
            }
            
            return nil
        }
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        item[HPLayoutConstants.kItemAmountKey] = NSDecimalNumber(double: 12.00)
        item[HPLayoutConstants.kItemDetailKey] = "Detail"
        item[HPLayoutConstants.kItemNameKey] = "Item1"
        item[HPLayoutConstants.kItemTimeIntervalKey] = NSNumber(double: NSDate().timeIntervalSince1970)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInsertItem() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let result = Item.insertItemInfo(item, inManagedObjectContext: managedObjectContext!)
        print("\(result)")
    }
    
    func testDeleteItem() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let result1 = Item.insertItemInfo(item, inManagedObjectContext: managedObjectContext!)
        print("\(result1)")
        
        let result = Item.deleteItemInfo(item, inManagedObjectContext: managedObjectContext!)
        print("\(result)")
    }
    
    func testAllItems() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let items = HPDetailController.items
        print("\(items)")
    }
    
    func testCurrentWeek() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        print("\(HPDetailController.itemsInCurrentWeek())")
    }
    
    func testCurrentMonth() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        print("\(HPDetailController.itemsInCurrentMonth())")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
